# Undo/Redo Buttons

This small extension for VSCode adds undo/redo buttons to the editor bar. They do the same as these default shortcuts:

| Command | Keybindings (Windows) |
| ------- | --------------------- |
| `Undo`  | `Ctl + z`             |
| `Redo`  | `Ctl + y`             |

Much thanks to Nikita Rudenko for providing the Back & Forth extension (https://github.com/nikita-rudenko/back-n-forth) which served as a template.
