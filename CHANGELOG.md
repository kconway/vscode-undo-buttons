# Change Log

## 1.1.1

- Fixed minor typos

## 1.1.0

- Updated icon for Marketplace

## 1.0.0

- Initial release
