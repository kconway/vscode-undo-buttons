import * as vscode from 'vscode';

function setup(context: vscode.ExtensionContext) {
  const undo = vscode.commands.registerCommand(
    `vscode-undo-button.undoButton`,
    () => {
      vscode.commands.executeCommand('undo');
    }
  );

  const redo = vscode.commands.registerCommand(
    `vscode-undo-button.redoButton`,
    () => {
      vscode.commands.executeCommand('redo');
    }
  );

  context.subscriptions.push(undo, redo);
}

export function activate(context: vscode.ExtensionContext) {
  setup(context);
}
export function deactivate() {}
